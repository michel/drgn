Source: drgn
Section: devel
Priority: optional
Maintainer: Michel Lind <michel@michel-slm.name>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 libdw-dev,
 libelf-dev,
 libkdumpfile-dev (>= 0.5.0-3),
 pkg-config,
 python3-dev,
 python3-setuptools,
 python3-sphinx,
 zstd <!nocheck>,
Standards-Version: 4.6.2
Homepage: https://github.com/osandov/drgn
Testsuite: autopkgtest-pkg-python
Vcs-Browser: https://salsa.debian.org/michel/drgn
Vcs-Git: https://salsa.debian.org/michel/drgn.git

Package: python-drgn-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Programmable debugger - documentation
 drgn (pronounced "dragon") is a debugger with an emphasis on programmability.
 .
 This package provides the documentation for drgn.

Package: python3-drgn
Section: python
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Suggests:
 python-drgn-doc,
Description: Programmable debugger
 drgn (pronounced "dragon") is a debugger with an emphasis on programmability.
 drgn exposes the types and variables in a program for easy, expressive
 scripting in Python.
